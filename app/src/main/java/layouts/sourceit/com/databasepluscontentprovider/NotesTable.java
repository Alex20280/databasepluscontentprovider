package layouts.sourceit.com.databasepluscontentprovider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class NotesTable implements Table{

    public static final String NAME = "notes";

    public interface Columns extends BaseColumns{
        String TITLE = "title";
        String TEXT = "text";
    }

    public static final String CREATE = "CREATE TABLE "
            + NAME
            + "("
            + Columns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Columns.TITLE + " TEXT, "
            + Columns.TEXT + " TEXT "
            + ");";

    public static final String DROP = "DROP TABLE IS EXISTS " + NAME;

    private  static final String MIME_TYPE = "vnd." + BuildConfig.AUTHORITY + "_" + NAME;

    private static final String CONTENT_PATH = "content://" + BuildConfig.AUTHORITY + "/" + NAME; //path to table

    public static final Uri CONTENT_URI = Uri.parse(CONTENT_PATH); //path to table in uri format


    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getColumnId() {
        return Columns._ID;
    }

    public String getContentItemType(){
        return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + MIME_TYPE;
    }
    public String getContentType(){
        return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + MIME_TYPE;
    }

    @Override
    public Uri getItemUri(long id) {
        return Uri.parse(CONTENT_PATH + "/" + id);
    }

    public interface ColumnIndices{
        int _ID = 0;
        int TITLE = 1;
        int TEXT = 2;
    }

    public interface Selection {
        String TITLE_BY_CONTENT = Columns.TITLE + " = ?";
    }
}
