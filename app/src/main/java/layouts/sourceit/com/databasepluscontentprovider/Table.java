package layouts.sourceit.com.databasepluscontentprovider;

import android.net.Uri;

public interface Table {

    String getName();

    String getColumnId();

    Uri getItemUri(long id);
}
