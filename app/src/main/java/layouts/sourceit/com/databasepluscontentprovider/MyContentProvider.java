package layouts.sourceit.com.databasepluscontentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;


public class MyContentProvider extends ContentProvider {

    public static final String AUTHORITY = BuildConfig.AUTHORITY; //what we wrote in gradle
    public static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    public final NotesTable notesTable = new NotesTable();

    static {
        URI_MATCHER.addURI(AUTHORITY,NotesTable.NAME, MappedUri.NOTES);
        URI_MATCHER.addURI(AUTHORITY,NotesTable.NAME + "/#", MappedUri.NOTES);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        database = new DataBaseHelper(getContext()).getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Table table = getTable(uri); //exact table we work with
        SQLiteQueryBuilder buider = new SQLiteQueryBuilder(); // makes quires to db
        buider.setTables(table.getName());
        if (isItemUri(uri)){ //
            buider.appendWhere(table.getColumnId() + "=" + uri.getLastPathSegment());
        }
        Cursor cursor = buider.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        setNotificationUri(cursor,uri); // monitors changes
        return cursor;
    }

    private void setNotificationUri(Cursor cursor, Uri uri) {
        Context context = getContext();
        if (context != null){
            cursor.setNotificationUri(context.getContentResolver(), uri);
        }
    }

    private boolean isItemUri(Uri uri) { //link to item or our table
        switch (URI_MATCHER.match(uri)){
            case MappedUri.NOTES_ITEM:
                return true;
        }
        return false;
    }

    private Table getTable(Uri uri) {
        switch (URI_MATCHER.match(uri)){
            case MappedUri.NOTES:
            case MappedUri.NOTES_ITEM:
                return notesTable;

            default:
                throw new IllegalArgumentException ("Uri: " + uri + " is not supported");
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)){
            case MappedUri.NOTES:
                return notesTable.getContentItemType();
            default:
                throw new IllegalArgumentException("Uri: " + uri + " is not supported");
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public interface MappedUri {
        int NOTES = 0;
        int NOTES_ITEM = 1;
    }

}
